/** @type {import('sequelize-cli').Migration} */

module.exports = {

  async up(queryInterface, Sequelize) {

    await queryInterface.createTable("users", {

      id: {

        type: Sequelize.DataTypes.INTEGER,

        primaryKey: true,

        allowNull: false,

        autoIncrement: true,

      },

      name: {

        type: Sequelize.DataTypes.STRING(50),

        allowNull: false,

      },

      email: {

        type: Sequelize.DataTypes.STRING(50),

        allowNull: false,

        unique: true,

      },

      password: {

        type: Sequelize.DataTypes.STRING(100),

        allowNull: false,

      },

      role: {

        type: Sequelize.ENUM("admin", "user"),

        allowNull: false,

        defaultValue: "user",

      },



      createdAt: {

        type: Sequelize.DataTypes.DATE,

        allowNull: false,

        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),

      },

      updatedAt: {

        type: Sequelize.DataTypes.DATE,

        allowNull: true,

      },

    });

  },




  async down(queryInterface, Sequelize) {

    await queryInterface.dropTable("users");

  },

};