module.exports = {

  development: {

    username: process.env.POSTGRES_ADMIN_USER,

    password: process.env.POSTGRES_ADMIN_PASS,

    //database: process.env.POSTGRES_HOST_NAME,

    host: process.env.POSTGRES_HOST_NAME,

    dialect: 'postgres',

  },

};
