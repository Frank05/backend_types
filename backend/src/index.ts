//Esto se elimina en el build
import express from "express";
import { connectedDB } from "./libs/connectedDB";
import routerApi from "./routes";

const app = express()


async function main() {
  try {

    await connectedDB(),
      routerApi(app);
    app.listen(3000, () => {
      console.log('Backend Server  ready!', 3000);
    });

  } catch (error) {

  }
}

main();
