import { Sequelize } from 'sequelize-typescript';
import { User } from "../models/users.models";


//console.log('VARIABLES:', a);

export async function connectedDB(): Promise<Sequelize> {
  console.log('VARIABLE', process.env.POSTGRES_HOST_NAME)
  const sequelize = new Sequelize(

    {
      host: process.env.POSTGRES_HOST_NAME!,
      dialect: 'postgres',
      username: process.env.POSTGRES_ADMIN_USER!,
      password: process.env.POSTGRES_ADMIN_PASS!,
    }

  );
  sequelize.addModels(
    [
      User
    ]
  )
  try {
    // Realiza la conexión a la base de datos
    await sequelize.authenticate();
    //await User.findAll();
    //const users = await User.findAll();
    //console.log('users:', users)
    console.log('Conexión exitosa a la base de datos');
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
    throw error;
  }

  return sequelize;
}