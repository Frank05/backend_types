import { Sequelize } from 'sequelize-typescript';
import { Dialect } from 'sequelize'

export async function connectedDB(): Promise<Sequelize> {

  const sequelize = new Sequelize({

    dialect: 'postgres',
    username: process.env.POSTGRES_USER! as string,
    password: process.env.POSTGRES_PASSWORD! as string,
    database: process.env.POSTGRES_NAME! as string,
    host: 'localhost',
    logging: false,
  })

  try {
    // Realiza la conexión a la base de datos
    await sequelize.authenticate();
    console.log('Conexión exitosa a la base de datos');
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
    throw error;
  }

  return sequelize;
}