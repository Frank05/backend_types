import express, { NextFunction, Request, Response } from 'express';




import UserService from '../services/users.services';

const userService = new UserService();




const router = express.Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {

  try {

    const customers = await userService.getAllUser();

    res.json(customers);

  } catch (error) {

    next(error);

  }

});




export default router;