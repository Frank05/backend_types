import express from 'express';




import userRouter from './user.router';

//mport transactionRouter from './transactions.router';




const routerApi = (app: express.Application) => {

  const router = express.Router();
  app.use('/api/v1', router);
  router.use('/users', userRouter);

  // router.use('/transactions', transactionRouter);

};




export default routerApi;