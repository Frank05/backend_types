"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const users_models_1 = require("../models/users.models");
class UserServices {
    async getAllUser() {
        const users = await users_models_1.User.findAll();
        return users;
    }
}
exports.default = UserServices;
//# sourceMappingURL=users.services.js.map