"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectedDB = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
async function connectedDB() {
    const sequelize = new sequelize_typescript_1.Sequelize({
        dialect: 'postgres',
        username: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_NAME,
        host: 'localhost',
        logging: false,
    });
    try {
        await sequelize.authenticate();
        console.log('Conexión exitosa a la base de datos');
    }
    catch (error) {
        console.error('Error al conectar a la base de datos:', error);
        throw error;
    }
    return sequelize;
}
exports.connectedDB = connectedDB;
//# sourceMappingURL=config.js.map