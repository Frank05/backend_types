"use strict";
const axios = require('axios');
async function connect_api() {
    const options = {
        method: 'GET',
        url: 'https://pokeapi.co/api/v2/pokemon/ditto',
    };
    try {
        const response = await axios.request(options);
        const pokemon = response.data;
        pokemon.forms.forEach((name) => {
            console.log('FORMS:', name);
        });
        const a = pokemon.abilities;
        console.log('Frank:', a);
        const prueba = pokemon.held_items.reduce((name, element) => {
            const valor = element.version_details.filter((item) => item.version.name === 'emerald');
            if (valor[0]?.version) {
                name = valor[0].version.name;
            }
            return name;
        }, '');
        console.log('Nombre_pockemon:', prueba);
    }
    catch (error) {
        console.error(error);
    }
}
connect_api();
module.exports = {
    connect_api
};
//# sourceMappingURL=connect_api.js.map