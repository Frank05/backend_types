"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const connectedDB_1 = require("./libs/connectedDB");
const routes_1 = __importDefault(require("./routes"));
const app = (0, express_1.default)();
async function main() {
    try {
        await (0, connectedDB_1.connectedDB)(),
            (0, routes_1.default)(app);
        app.listen(3000, () => {
            console.log('Backend Server  ready!', 3000);
        });
    }
    catch (error) {
    }
}
main();
//# sourceMappingURL=index.js.map