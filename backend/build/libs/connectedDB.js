"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.connectedDB = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const users_models_1 = require("../models/users.models");
async function connectedDB() {
    console.log('VARIABLE', process.env.POSTGRES_HOST_NAME);
    const sequelize = new sequelize_typescript_1.Sequelize({
        host: process.env.POSTGRES_HOST_NAME,
        dialect: 'postgres',
        username: process.env.POSTGRES_ADMIN_USER,
        password: process.env.POSTGRES_ADMIN_PASS,
    });
    sequelize.addModels([
        users_models_1.User
    ]);
    try {
        await sequelize.authenticate();
        console.log('Conexión exitosa a la base de datos');
    }
    catch (error) {
        console.error('Error al conectar a la base de datos:', error);
        throw error;
    }
    return sequelize;
}
exports.connectedDB = connectedDB;
//# sourceMappingURL=connectedDB.js.map