"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const users_services_1 = __importDefault(require("../services/users.services"));
const userService = new users_services_1.default();
const router = express_1.default.Router();
router.get('/', async (req, res, next) => {
    try {
        const customers = await userService.getAllUser();
        res.json(customers);
    }
    catch (error) {
        next(error);
    }
});
exports.default = router;
//# sourceMappingURL=user.router.js.map